package org.edec.student.factSheet.ctrl;

import org.edec.factSheet.service.FactSheetService2;
import org.edec.factSheet.service.impl.FactSheetServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexPageCtrlTest {

    private FactSheetService2 factSheetService = new FactSheetServiceImpl();

    @Test
    public void orderTest() {
        boolean actual = factSheetService.addFactSheet(Long.valueOf(219362), 5, true,
                true, "КИ15-16Б");
        assertTrue(actual);
    }

    @Test
    public void updateStatusTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean actual = factSheetService.updateStatus(1, Long.valueOf(946));
        assertTrue(actual);
    }

    @Test
    public void updateCompletionTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean actual = factSheetService.updateCompletion(Long.valueOf(946));
        assertTrue(actual);
    }

    @Test
    public void updateReceiptTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean actual = factSheetService.updateReceipt(Long.valueOf(946));
        assertTrue(actual);
    }

    @Test
    public void deleteFactSheetTest() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean actual = factSheetService.deleteFactSheet(Long.valueOf(946));
        assertTrue(actual);
    }
}